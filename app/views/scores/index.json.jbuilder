json.array!(@scores) do |score|
  json.extract! score, :id, :course_id, :value
  json.url score_url(score, format: :json)
end
