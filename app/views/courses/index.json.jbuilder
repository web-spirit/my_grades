json.array!(@courses) do |course|
  json.extract! course, :id, :name, :rate, :semester, :genre, :mandatory
  json.url course_url(course, format: :json)
end
