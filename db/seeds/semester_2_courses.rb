courses = [
	{ name: 'Μαθηματική Ανάλυση ΙΙ', rate: 4, semester: '2', genre: 'Κ', mandatory: true },
	{ name: 'Διακριτά Μαθηματικά', rate: 4, semester: '2', genre: 'Κ', mandatory: true },
	{ name: 'Στατιστική', rate: 4, semester: '2', genre: 'Κ', mandatory: true },
	{ name: 'Στοχαστικές Ανελίξεις', rate: 4, semester: '2', genre: 'Κ', mandatory: true },
	{ name: 'Λειτουργικά Συστήματα Ι', rate: 5, semester: '2', genre: 'Κ', mandatory: true },
	{ name: 'Αντικειμενοστρεφής Προγραγραμματισμός', rate: 5, semester: '2', genre: 'Κ', mandatory: true },
	{ name: 'Ανάλυση και Σχεδιασμός Συστημάτων', rate: 4, semester: '2', genre: 'Κ', mandatory: true }
]

courses.each do |course|
	Course.create(course)
end