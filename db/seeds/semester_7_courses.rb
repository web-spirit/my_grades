courses = [

	{ name: 'Πτυχιακή Εργασία', rate: 5, semester: '7', genre: 'Κ', mandatory: true },
	{ name: 'Διοίκηση Έργων', rate: 5, semester: '7', genre: 'Κ', mandatory: true },
	{ name: 'Επικοινωνίες Πολυμέσων', rate: 5, semester: '7', genre: 'K' mandatory: true },
	{ name: 'Συστήματα Ηλεκτρονικής Μάθησης', rate: 5, semester: '7', genre: 'ΗΥ' mandatory: true },
	{ name: '', rate: 5, semester: '7', genre: 'K' mandatory: true },
]
courses.each do |course|
	Course.create(course)
end