courses = [
	{ name: ' Πρωτόκολλα Διαδικτύου', rate: 4, semester: '6', genre: 'Κ', mandatory: true },
	{ name: 'Πληροφοριακά Συστήματα', rate: 4, semester: '6', genre: 'Κ', mandatory: true },
	{ name: 'Ασφάλεια Πληροφοριακών Συστημάτων', rate: 5, semester: '6', genre: 'HY', mandatory: true },
	{ name: 'Αποθήκες και Εξόρυξη Δεδομένων', rate: 5, semester: '6', genre: 'HY', mandatory: true },
	{ name: 'Ψηφιακή Επεξεργασία Σήματος', rate: 4, semester: '6', genre: 'HY', mandatory: false },
	{ name: 'Εφαρμογές Ψηφιακών Μέσων στην Εκπαίδευση', rate: 4, semester: '6', genre: 'HY', mandatory: false },
	{ name: ' Ηλεκτρονικές Υπηρεσίες Υγείας', rate: 4, semester: '6', genre: 'K', mandatory: false },
	{ name: 'Διδακτική Ψηφιακών Τεχνολογιών', rate: 4, semester: '6', genre: 'HY', mandatory: false },
	{ name: 'Συμβουλευτικές Υπηρεσίες', rate: 4, semester: '6', genre: 'K', mandatory: false },
	{ name: 'Κοινωνικά Δίκτυα', rate: 4, semester: '6', genre: 'K', mandatory: false },
	{ name: 'Επιχειρησιακή Έρευνα', rate: 4, semester: '6', genre: 'K', mandatory: false },
	{ name: 'Διοίκηση Ολικής Ποιότητας', rate: 4, semester: '6', genre: 'K', mandatory: false },
	{ name: 'Ανάκτηση Πληροφοριών', rate: 4, semester: '6', genre: 'K', mandatory: false },
	{ name: 'Ψηφιακή Επεξεργασία Εικόνας', rate: 4, semester: '6', genre: 'K', mandatory: false },
	{ name: 'Τεχνικές Βελτιστοποίησης', rate: 4, semester: '6', genre: 'K', mandatory: false },
	{ name: 'Ασύρματες Επικοινωνίες', rate: 4, semester: '6', genre: 'SED', mandatory: true },
	{ name: 'Ασφάλεια Δικτύων', rate: 4, semester: '6', genre: 'SED', mandatory: true },

	{ name: 'Ευφυή Δίκτυα', rate: 4, semester: '6', genre: 'SED', mandatory: false }
	]
courses.each do |course|
	Course.create(course)
end