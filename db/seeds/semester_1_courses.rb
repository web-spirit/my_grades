# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
courses = [
	# semester 1
	{name:'Ανάλυση 1', rate: 4, semester:'1', genre:'Κ', mandatory:true},
	{name:'Πιθανότητες', rate:4 , semester:'1', genre:'Κ', mandatory:true},
	{name:'Γραμμική', rate:4 , semester:'1', genre:'Κ', mandatory:true},
	{name:'Μαθηματική Λογική', rate:4 , semester:'1', genre:'Κ', mandatory:true},
	{name:'Αρχιτεκτονικές Υπολογιστών', rate:5 , semester:'1', genre:'Κ', mandatory:true},
	{name:'C', rate:5 , semester:'1', genre:'K', mandatory:true},
	{name:'Διδακτική Μεθοδολογία', rate:4 , semester:'1', genre:'Κ', mandatory:true},
]

courses.each do |course|
	Course.create(course)
end