courses = [
	
	{ name: 'Τεχνιτή Νοημοσύνη', rate: 4, semester: '5', genre: 'Κ', mandatory: true },
	{ name: 'Δίκτυα Υπολογιστών ΙΙ', rate: 4, semester: '5', genre: 'Κ', mandatory: true },
	{ name: 'Πολιτικές και Διαχείριση Ασφάλειας', rate: 4, semester: '5', genre: 'Κ', mandatory: true },
	{ name: 'Βάσεις Δεδομένων', rate: 4, semester: '5', genre: 'Κ', mandatory: true },
	{ name: 'Συστήματα Ροής Εργασίας', rate: 5, semester: '5', genre: 'ΗΥ', mandatory: true },
	{ name: 'Σημασιολογικός Ιστός - XML', rate: 5, semester: '5', genre: 'ΗΥ', mandatory: true },
	{ name: 'Γραφικά Υπολογιστών & Εικονική Πραγματικότητα', rate: 4, semester: '5', genre: 'ΗΥ/ΣΕΣ', mandatory: false },
	{ name: 'Μεταγλωττιστές', rate: 4, semester: '5', genre: 'ΗΥ/ΣΕΔ', mandatory: false },
	{ name: 'Αναγνώριση Προτύπων', rate: 4, semester: '5', genre: 'ΗΥ/ΣΕΔ', mandatory: false },
	{ name: 'Εκπαιδευτική Ψυχολογία', rate: 4, semester: '5', genre: 'ΗΥ', mandatory: false },
	{ name: 'Ψηφιακά Συστήματα στην Εκπαίδευση', rate: 4, semester: '5', genre: 'ΗΥ', mandatory: false },
	{ name: 'Management', rate: 4, semester: '5', genre: 'HY/ΣΕΔ', mandatory: false },
	{ name: 'Marketing', rate: 4, semester: '5', genre: 'HY/ΣΕΔ', mandatory: false },
	{ name: 'Διαχείριση Τεχνολογίας και Καινοτομίας', rate: 4, semester: '5', genre: 'ΗΥ/ΣΕΔ', mandatory: false },
	{ name: 'Σύματα και Συστήματα', rate: 4, semester: '5', genre: 'ΣΕΔ', mandatory: false },
	{ name: 'Ψηφιακές Επικοινωνίες', rate: 5, semester: '5', genre: 'ΣΕΔ', mandatory: true },
	{ name: 'Συστήματα Ουρών Αναμονής', rate: 5, semester: '5', genre: 'ΣΕΔ', mandatory: true },
	{ name: 'Σύματα και Συστήματα', rate: 4, semester: '5', genre: 'ΣΕΔ', mandatory: false },
	{ name: 'Προσομοίωση Τηλεπικοινωνιακών Συστημάτων και Δικτύων', rate: 4, semester: '5', genre: 'ΣΕΔ', mandatory: false },
	
]

courses.each do |course|
	Course.create(course)
end