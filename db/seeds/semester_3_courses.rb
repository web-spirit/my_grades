courses = [
	{name:'Λειτουργικά 2', rate:5 , semester:'3', genre:'K', mandatory:true},
	{name:'Εισαγωγή στις Τηλεπικοινωνίες', rate:5 , semester:'3', genre:'K', mandatory:true},
	{name:'Δομές Δεδομένων', rate:5 , semester:'3', genre:'K', mandatory:true},
	{name:'Τεχνολογία Λογισμικού', rate:5 , semester:'3', genre:'K', mandatory:true},
	{name:'Συστήματα Αλληλεπίδρασης', rate:5 , semester:'3', genre:'K', mandatory:true},
	{name:'Θεωρία Πληροφορίας', rate:5 , semester:'3', genre:'K', mandatory:true}
]

courses.each do |course|
	Course.create(course)
end