courses = [
	{ name: ' Αλγόριθμοι και Πολυπλοκότητα', rate: 5, semester: '4', genre: 'Κ', mandatory: true },
	{ name: 'Κατανεμημένα Συστήματα', rate: 5, semester: '4', genre: 'Κ', mandatory: true },
	{ name: 'Δίκτυα Υπολογιστών Ι', rate: 5, semester: '4', genre: 'Κ', mandatory: true },
	{ name: 'Τεχνολογία Πολυμέσων', rate: 5, semester: '4', genre: 'Κ', mandatory: true },
	{ name: 'Σχεδιασμός Βάσεων Δεδομένων', rate: 5, semester: '4', genre: 'Κ', mandatory: true },
	{ name: 'Προγραμματισμός Παγκόσμιου Ίστου', rate: 5, semester: '4', genre: 'Κ', mandatory: true },
	]
courses.each do |course|
	Course.create(course)
end