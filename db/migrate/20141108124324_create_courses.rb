class CreateCourses < ActiveRecord::Migration
  def change
    create_table :courses do |t|
      t.string :name
      t.float :rate
      t.string :semester
      t.string :genre
      t.boolean :mandatory

      t.timestamps
    end
  end
end
