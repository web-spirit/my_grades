class CreateScores < ActiveRecord::Migration
  def change
    create_table :scores do |t|
      t.integer :course_id
      t.float :value

      t.timestamps
    end
    add_index :scores, :course_id
  end
end
